/*
** threads.h for malloc in /home/gravie_j/Documents/projets/PSU_2013_malloc
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Thu Feb 13 10:56:29 2014 Jean Gravier
** Last update Thu Feb 13 20:07:30 2014 Jean Gravier
*/

#ifndef THREADS_H_
# define THREADS_H_

#include <pthread.h>

void	malloc_lock_mutex();
void	malloc_unlock_mutex();
void	realloc_lock_mutex();
void	realloc_unlock_mutex();
void	free_lock_mutex();
void	free_unlock_mutex();

#endif /* !THREADS_H_ */
