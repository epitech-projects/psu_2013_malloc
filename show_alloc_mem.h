/*
** show_alloc_mem.c for malloc in /home/degreg_e/rendu/PSU_2013_malloc
** 
** Made by enzo de gregorio
** Login   <degreg_e@epitech.net>
** 
** Started on  Fri Feb 14 15:44:24 2014 enzo de gregorio
** Last update Fri Feb 14 15:48:03 2014 enzo de gregorio
*/

#ifndef SHOW_ALLOC_MEM_H_
# define  SHOW_ALLOC_MEM_H_

void	show_alloc_mem();

#endif
