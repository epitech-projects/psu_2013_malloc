/*
** free.h for malloc in /home/gravie_j/Documents/projets/PSU_2013_malloc
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Mon Feb 10 22:45:30 2014 Jean Gravier
** Last update Thu Feb 13 17:54:34 2014 Jean Gravier
*/

#ifndef FREE_H_
# define FREE_H_

# include "malloc.h"

void		free(void *);
void		merge_blocks(t_blocklist *, t_blocklist *);

#endif /* !FREE_H_ */
