/*
** free_threads.c for malloc in /home/gravie_j/Documents/projets/PSU_2013_malloc
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Thu Feb 13 20:04:20 2014 Jean Gravier
** Last update Fri Feb 14 16:24:26 2014 Jean Gravier
*/

#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include "threads.h"

static pthread_mutex_t g_mutex_free = PTHREAD_MUTEX_INITIALIZER;

void	free_lock_mutex()
{
  if (pthread_mutex_lock(&g_mutex_free))
    {
      puts("lock free mutex error.");
      abort();
    }
}

void	free_unlock_mutex()
{
  if (pthread_mutex_unlock(&g_mutex_free))
    {
      puts("unlock free mutex error.");
      abort();
    }
}
