/*
** free.c for malloc in /home/gravie_j/Documents/projets/PSU_2013_malloc
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Mon Feb 10 22:22:23 2014 Jean Gravier
** Last update Fri Feb 14 18:08:22 2014 Jean Gravier
*/

#include <unistd.h>
#include <stdio.h>
#include "free.h"
#include "malloc.h"
#include "threads.h"

void		free(void *ptr)
{
  t_blocklist	*block;

  if (ptr == NULL)
    return ;
  free_lock_mutex();
  block = g_firstblock;
  while (block != NULL)
    {
      if (ptr == block->ptr)
	{
	  block->is_free = TRUE;
	  free_unlock_mutex();
	  return ;
	}
      block = block->next;
    }
  free_unlock_mutex();
}
