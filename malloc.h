/*
** malloc.h for malloc in /home/gravie_j/Documents/projets/PSU_2013_malloc
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Mon Feb 10 22:26:17 2014 Jean Gravier
** Last update Fri Feb 14 16:51:19 2014 Jean Gravier
*/

#ifndef MALLOC_H_
# define MALLOC_H_

# define FALSE 0
# define TRUE 1
# define align(size) (size % 8 ? size + (8 - (size % 8)) : size + (8 % size))

typedef struct		s_blocklist
{
  size_t		size;
  void			*ptr;
  int			is_free;
  struct s_blocklist	*next;
  struct s_blocklist	*prev;
} t_blocklist;

t_blocklist		*g_firstblock;

void			*malloc(size_t size);

#endif /* !MALLOC_H_ */
