/*
** malloc_threads.c for malloc in /home/gravie_j/Documents/projets/PSU_2013_malloc
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Thu Feb 13 20:02:21 2014 Jean Gravier
** Last update Thu Feb 13 20:02:59 2014 Jean Gravier
*/

#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include "threads.h"

static pthread_mutex_t g_mutex_malloc = PTHREAD_MUTEX_INITIALIZER;

void	malloc_lock_mutex()
{
  if (pthread_mutex_lock(&g_mutex_malloc))
    {
      puts("lock malloc mutex error.");
      abort();
    }
}

void	malloc_unlock_mutex()
{
  if (pthread_mutex_unlock(&g_mutex_malloc))
    {
      puts("unlock malloc mutex error.");
      abort();
    }
}

