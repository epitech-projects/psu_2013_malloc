/*
** calloc.h for malloc in /home/gravie_j/Documents/projets/PSU_2013_malloc
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Thu Feb 13 15:16:37 2014 Jean Gravier
** Last update Thu Feb 13 15:44:03 2014 Jean Gravier
*/

#ifndef CALLOC_H_
# define CALLOC_H_

# include <stdlib.h>

void	*calloc(size_t, size_t);

#endif /* !CALLOC_H_ */
