/*
** malloc.c for malloc in /home/gravie_j/Documents/projets/PSU_2013_malloc
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Mon Feb 10 15:39:06 2014 Jean Gravier
** Last update Fri Feb 14 18:01:24 2014 Jean Gravier
*/

#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>
#include "malloc.h"
#include "threads.h"

void		block_init(t_blocklist *block)
{
  block->size = 0;
  block->ptr = NULL;
  block->next = NULL;
  block->prev = NULL;
  block->is_free = FALSE;
}

t_blocklist	*block_create(size_t size)
{
  t_blocklist	*block;
  void		*breakpoint;

  breakpoint = sbrk(0);
  block = breakpoint;
  if (sbrk(size + sizeof(t_blocklist)) == (void *) -1)
    return (NULL);
  block_init(block);
  block->size = size;
  block->ptr = breakpoint + sizeof(t_blocklist);
  return (block);
}

void			*malloc(size_t size)
{
  t_blocklist		*temp;
  t_blocklist		*previous;

  if (size <= 0)
    return (NULL);
  malloc_lock_mutex();
  size = align(size);
  temp = NULL;
  if (g_firstblock != NULL)
    {
      temp = g_firstblock;
      while (temp != NULL && temp->next != NULL)
	{
	  previous = temp;
	  temp = temp->next;
	}
      temp->next = block_create(size);
      temp = temp->next;
      temp->prev = previous;
    }
  else
    {
      g_firstblock = block_create(size);
      temp = g_firstblock;
    }
  malloc_unlock_mutex();
  return (temp->ptr);
}
