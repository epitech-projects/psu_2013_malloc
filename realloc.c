/*
** realloc.c for malloc in /home/gravie_j/Documents/projets/PSU_2013_malloc
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Tue Feb 11 12:53:32 2014 Jean Gravier
** Last update Fri Feb 14 17:42:43 2014 Jean Gravier
*/

#include <unistd.h>
#include <string.h>
#include "malloc.h"
#include "threads.h"
#include "free.h"

void		*realloc(void *ptr, size_t size)
{
  t_blocklist	*temp;
  void		*new_ptr;

  realloc_lock_mutex();
  new_ptr = NULL;
  if (ptr == NULL && size)
    {
      realloc_unlock_mutex();
      return (malloc(size));
    }
  else if (!size)
    {
      free(ptr);
      realloc_unlock_mutex();
      return (NULL);
    }
  size = align(size);
  temp = g_firstblock;
  while (temp != NULL)
    {
      if (ptr == temp->ptr)
	{
	  new_ptr = malloc(size);
	  if (size <= temp->size)
	    new_ptr = memcpy(new_ptr, temp->ptr, size);
	  else
	    new_ptr = memcpy(new_ptr, temp->ptr, temp->size);
	  realloc_unlock_mutex();
	  return (new_ptr);
	}
      temp = temp->next;
    }
  realloc_unlock_mutex();
  return (ptr);
}
