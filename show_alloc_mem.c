/*
** show_alloc_mem.c for malloc in /home/degreg_e/rendu/PSU_2013_malloc
** 
** Made by enzo de gregorio
** Login   <degreg_e@epitech.net>
** 
** Started on  Fri Feb 14 15:49:09 2014 enzo de gregorio
** Last update Fri Feb 14 17:05:16 2014 enzo de gregorio
*/

#include <stdio.h>
#include <unistd.h>
#include "malloc.h"
#include "show_alloc_mem.h"

void		show_alloc_mem()
{
  t_blocklist	*temp;
  
  printf("break : %p\n", sbrk(0));
  temp = g_firstblock;
  while (temp != NULL)
    {
      if (temp->is_free == FALSE)
	printf("%p - %p : %lu octets\n", temp->ptr,
	       (void *)(temp->ptr + temp->size), temp->size);
      temp = temp->next;
    }
  return ;
}
