/*
** calloc.c for malloc in /home/gravie_j/Documents/projets/PSU_2013_malloc
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Thu Feb 13 15:16:28 2014 Jean Gravier
** Last update Fri Feb 14 17:39:17 2014 Jean Gravier
*/

#include <stdlib.h>
#include <stdio.h>
#include "malloc.h"

void		*calloc(size_t nmemb, size_t size)
{
  size_t	temp;

  if (!nmemb || !size)
    return (NULL);

  size = align(size);

  temp = nmemb * size;
  if (nmemb == temp / size)
    return (malloc(temp));
  return (NULL);
}
