##
## Makefile for malloc in /home/gravie_j/Documents/projets/PSU_2013_malloc
##
## Made by Jean Gravier
## Login   <gravie_j@epitech.net>
##
## Started on  Mon Feb 10 15:44:16 2014 Jean Gravier
## Last update Fri Feb 14 18:08:02 2014 Jean Gravier
##

CC=		gcc

RM=		rm -f

NAME=		libmy_malloc_$$HOSTTYPE.so

LINK=		libmy_malloc.so

SRCS=		malloc.c \
		free.c \
		realloc.c \
		malloc_threads.c \
		realloc_threads.c \
		free_threads.c \
		calloc.c \
		show_alloc_mem.c \

OBJS=		$(SRCS:.c=.o)

CFLAGS +=	-Wall -Wextra -Werror -fPIC

$(NAME): $(OBJS)
	$(CC) $(OBJS) -shared -o $(NAME)
	ln -sf ./$(NAME) $(LINK)

all: $(NAME)

clean:
	$(RM) $(OBJS)
	$(RM) $(LINK)

fclean: clean
	$(RM) $(NAME)

re: fclean all
