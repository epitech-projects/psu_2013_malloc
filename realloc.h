/*
** realloc.h for malloc in /home/gravie_j/Documents/projets/PSU_2013_malloc
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Tue Feb 11 12:54:35 2014 Jean Gravier
** Last update Tue Feb 11 12:55:05 2014 Jean Gravier
*/

#ifndef REALLOC_H_
# define REALLOC_H_

# include <unistd.h>

void	*realloc(void *, size_t);

#endif /* !REALLOC_H_ */
